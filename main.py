#!/bin/env python
# -*- encoding: utf-8 -*-

import json
import os
from bson import ObjectId
from bson.json_util import dumps
from tornado.options import define, parse_command_line
#import pymongo
from pymongo import MongoClient
import tornado.ioloop
import tornado.web

static_url_='/static/'

class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.render("template.html", Title_='{{t.Title}}',static_url_=static_url_)

class TaskHandler(tornado.web.RequestHandler):
    """ Implementa el API """

    def get(self):
        """ Metodo GET para listar las tareas """
        tasks = self.application.db.task.find()
        self.write(dumps({'Tasks': tasks}))

    def post(self):
        """ Metodo POST para crear tareas """
        title = self.get_argument('Title')
        if title:
            task = {'Title': title,
                    'Done': False}
            self.application.db.task.insert(task)

        self.write("Hello, world")

    def put(self, task_id):
        """ Metodo PUT para actualizar el estado de las tareas """
        task_id = ObjectId(task_id)
        print(task_id)
        data = json.loads(self.request.body)
        print(data)
        self.application.db.task.update({'_id': task_id}, {'$set': {'Done': data['Done']}})

    def delete(self):
        task = application.db.task
        for task_ in task.find({'Done':True}):
            task.remove(task_['_id'])

static_path='static'

settings = {
    "template_path": os.path.join(os.path.dirname(__file__), "templates"),
    "debug" : True
}
#    "static_path": os.path.join(os.path.dirname(__file__), "static"),
application = tornado.web.Application([
    (r'/', MainHandler),
    (r'/task/', TaskHandler),
    (r'/task/(?P<task_id>.+)', TaskHandler),
    (r'/(favicon.png)', tornado.web.StaticFileHandler, {"path": "static"}),
    (r'/(favicon.ico)', tornado.web.StaticFileHandler, {"path": "static"}),
    (r'/static/(.*)', tornado.web.StaticFileHandler, {'path': static_path}),
],**settings)

if __name__ == "__main__":
    print("server")
    print('Listening on http://localhost:8000')
    connection = MongoClient("mongodb://tododb:ABCDabcd1@ds037498.mongolab.com:37498/silvia")
    application.db = connection['silvia']
    port = int(os.environ.get("PORT", 5000))
    application.listen(port)
    tornado.ioloop.IOLoop.instance().start()
